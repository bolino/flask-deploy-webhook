## How to

1. Create keys for "www-data" user (with sudo -u www-data ssh-keygen etc.) and add them to GitHub Bitcket so that www-data can git pull. Keys will be created in a /.ssh folder in /var/www/ by default.
2. Give rights to www-data to restart Apache in sudoers file (visudo, see https://serverfault.com/questions/919136/restart-or-reload-apache-as-www-data-user-for-a-deploy-webhook#comment1190149_919141)
3. In script, use "sudo /etc/init.d/apache2 reload" for restart Apache
4. Create vhost for Flask webhook endpoint with:
	* # Necessary when using a Python virtualenv
	WSGIDaemonProcess webhook python-path=/var/www/webhook:/var/virtualenvs/flask-venv/lib/python3.4/site-packages
	* # Separate WSGI processes for Django+Flask co-running	
	WSGIProcessGroup webhook
	* # Public URL for Python app + Path to the corresponding wsgi script
	WSGIScriptAlias /webhook /var/www/webhook/flaskwebhook.wsgi
	* # Flask Webhook static files
	Alias /flask/static/ /var/www/webhook/webhook/static/
	<Directory "/var/www/webhook/">
		AllowOverride All
		Require all granted
	</Directory>