import logging
from sys import stderr, hexversion
logging.basicConfig(stream=stderr)
from logging.handlers import RotatingFileHandler

from json import loads, dumps
from subprocess import Popen, PIPE, STDOUT
from tempfile import mkstemp
from datetime import datetime
from os import environ, remove, fdopen
from os.path import isfile, basename

import requests
from ipaddress import ip_address, ip_network
from flask import Flask, request, abort, Response

app = Flask(__name__)

### SETTINGS
LOGFILE = '/var/www/webhook/webhook.log' # full path to log file
GITHUB_IP_WHITELIST = requests.get('https://api.github.com/meta').json()['hooks']
BITBUCKET_IP_WHITE_LIST = ['34.198.203.127','34.198.178.64','34.198.32.85']
STAGING_PATH = '/var/www/staging/'
PRODUCTION_PATH = '/var/www/production/'
# Dont forget to create keys for "www-data" user (with sudo -u www-data ssh-keygen) and add them to GitHub Bitcket so that www-data can git pull
# Don't forget to give rights to www-data to restart Apache in sudoers file (visudo, see https://serverfault.com/questions/919136/restart-or-reload-apache-as-www-data-user-for-a-deploy-webhook#comment1190149_919141)
# In script, use "sudo /etc/init.d/apache2 reload" for restart Apache

### Logging
log_handler = RotatingFileHandler(LOGFILE, maxBytes=80000, backupCount=1)
log_handler.setLevel(logging.INFO)
app.logger.setLevel(logging.INFO)
app.logger.addHandler(log_handler)

### Function to have readable millisecond time in log file
def logtime():
	return(datetime.utcnow().strftime('%d/%m %H:%M:%S/%f')[:17])


@app.route('/', methods=['GET', 'POST']) # Endpoint for receiving webhooks from github and bitbucket
def webhook():

	if request.method == 'POST':

		app.logger.info('######################################\n')
		app.logger.info(logtime() + '  POST request received...\n')

		# Get JSON payload
		try:
			payload = request.get_json()
		except Exception:
			app.logger.info(logtime() + '  ### ERROR ### Parsing JSON failed.\n')
			abort(400)
		# app.logger.info(payload) # testing

		# Test if valid IP and gather data from JSON
		src_ip = ip_address(u'{}'.format(request.remote_addr))  # Fix stupid ipaddress issue
		# src_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr
		try: # GitHub
			# Test if IP is within GitHub IP range
			for valid_ip in GITHUB_IP_WHITELIST:
				if src_ip in ip_network(valid_ip):
					source = "GitHub"
					break # it is a github ip, breaks loop and continue
			else:
				abort(403) # it is not a GitHub IP, abort
			app.logger.info(logtime() + "  JSON POST request received from GitHub IP...")
			# Gather data
			try:
				repo_name = payload['repository']['name']
				commit_author = payload["sender"]["login"] # or payload["pusher"]["name"] ?
				commit_branch = payload["ref"].replace("refs/heads/", "")
			except:
				app.logger.error(logtime() + "  ### ERROR ### POST request received from GitHub, but invalid JSON format.")
				return Response("Invalid JSON request.", status=400, mimetype='text/html')
		except: # BitBucket
			# Test if IP is within BitBucket IP range
			for valid_ip in BITBUCKET_IP_WHITE_LIST:
				if src_ip in ip_network(valid_ip):
					source = "BitBucket"
					break # it is a bitbucket ip, breaks loop and continue
			else:
				abort(403) # it is not a Bitbucket IP, abort
			app.logger.info(logtime() + "  JSON POST request received from BitBucket IP...")
			# Gather data
			try:
				repo_name = payload["repository"]["name"]
				commit_author = payload["actor"]["username"]
				commit_branch = payload["push"]["changes"][0]["new"]["name"]
				# commit_url = payload["push"]["changes"][0]["new"]["target"]["links"]["html"]["href"]
			except:
				app.logger.error(logtime() + "  ### ERROR ### POST request received from BitBucket, but invalid JSON format.")
				return Response("Invalid JSON request.", status=400, mimetype='text/html')

		# # Secret
		# if SECRET:
		# 	# Only SHA1 is supported
		# 	header_signature = request.headers.get('X-Hub-Signature')
		# 	if header_signature is None:
		# 		abort(403)
		# 	sha_name, signature = header_signature.split('=')
		# 	if sha_name != 'sha1':
		# 		abort(501)
		# 	# HMAC requires the key to be bytes, but data is string
		# 	mac = hmac.new(str(secret), msg=request.data, digestmod='sha1')
		# 	else:
		# 		# What compare_digest provides is protection against timing
		# 		# attacks; we can live without this protection for a web-based
		# 		# application
		# 		if not str(mac.hexdigest()) == str(signature):
		# 			abort(403)

		# Answer to GitHub ping request
		event = request.headers.get('X-GitHub-Event', 'ping')
		if event == 'ping':
			app.logger.info(logtime() + '  Ping received from GitHub.\n')
			return dumps({'msg': 'pong!'})

		# Answer to BitBucket ping request (NOT TESTED)
		event = request.headers.get('X-Event-Key', 'ping')
		if event == 'ping':
			app.logger.info(logtime() + '  Ping received from BitBucket.\n')
			return dumps({'msg': 'pong!'})

		app.logger.info(logtime() + '  Valid payload received from {} for project {}! {} just pushed on branch {}.\n'.format(source, repo_name, commit_author, commit_branch))

		# Build script path
		if commit_branch == 'staging':
			script_file = STAGING_PATH + '{}.sh'.format(repo_name)
		if commit_branch == 'master':
			script_file = PRODUCTION_PATH + '{}.sh'.format(repo_name)
		else:
			error_message = 'Branch {} is neither staging nor master. Not running any script.\n'.format(commit_branch)
			app.logger.info(logtime() + "  " + error_message)
			return Response(error_message, status=400, mimetype='text/html')

		# Test if script exists
		if not isfile(script_file):
			error_message = "### ERROR ### deploy script {} does not exist or is not a file.\n".format(script_file)
			app.logger.info(logtime() + "  " + error_message)
			return Response(error_message, status=500, mimetype='text/html')

		# Run script
		app.logger.info(logtime() + '  Executing deploy script {} ...\n'.format(script_file))
		proc = Popen(script_file, stdout=PIPE, stderr=PIPE, shell=True)
		stdout, stderr = proc.communicate()
		ran = {}
		ran[basename(script_file)] = {
			'returncode': proc.returncode,
			'stdout': stdout.decode('utf-8'),
			'stderr': stderr.decode('utf-8'),
		}	

		# Log errors if the script failed
		if proc.returncode != 0:
			# logging.error('{} : {} \n{}'.format(script_file, proc.returncode, stderr))
			error_message = '### ERROR ### code {} while executing {} : \n{}'.format(proc.returncode, script_file, stderr)
			app.logger.info(logtime() + "  " + error_message)
			abort(500)

		# Send OK code
		app.logger.info(logtime() + '  Script {} has been executed (hopefully).\n'.format(script_file))
		return Response('Success!', status=200, mimetype='text/html')

	elif request.method == 'GET': # Throw an error but display a HTML message for humans when browsing the endpoint
		return Response("<h3>This is a flask webhook endpoint. It's working and listening. This message is displayed because your request is not the one attended by the server.</h3>", status=405, mimetype='text/html')

	else:
		abort(405)


### MAIN
if __name__ == '__main__':
	#	app.run(host='0.0.0.0', port=5000, debug=True)
	app.run()

